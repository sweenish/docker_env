# Mr. Sweeney's C++ and Python Environment (Wichita State)
This is not an official environment for the programming courses at Wichita
State University. I created this environment for a faster and more consistent
environment that students can install and use. I also wanted something I
controlled for grading purposes.

It is possible to complete all the required coursework using the computers in
the lab, but it may be desirable to set up a coding environment on your own
machine. This guide does not hold your hand, but gives you the main steps. Any
gaps (like changing what font your terminal app uses) can be filled with an
Internet search.

## Pre-Work
* Get a better Terminal Emulator.
    * On Windows, get Windows Terminal from the Microsoft Store.
    * On Mac, you may be able to get away with using Terminal.app, but I would recommend iTerm2.
* Download a [Nerd Font](https://www.nerdfonts.com) and ensure your terminal is using it.
    * I use Sauce Code Pro.
* Create a folder where you will be storing your homework submissions.
    * If you are on Windows, I recommend creating a folder in the root of your C drive.
    * Something like C:\Courses\211\homework

## Installations
### git
* Download and install [git](https://git-scm.com).
    * Windows users: git can optionally be installed through a package manager like chocolatey or
    scoop.
    * Mac users: git can optionally be installed through a package manager like homebrew.
    * Benefits of using a package manager include ease of updating; git does not auto-update.
        * Keeping git up to date is not necessary for this course.
    * You can ensure git is installed correctly and functioning by cloning this repository.
        * Issue the following command in the terminal
        `git clone https://gitlab.com/sweenish/docker_env.git`

### Docker
* Download and install [Docker Desktop](https://www.docker.com/get-started).
* Test your install by running the following command in a terminal: `docker run hello-world`
    * You may need to reboot after installing Docker.
    * If the test fails, you may need to reboot your computer and try again.

### VS Code
* Download and install [Visual Studio Code](https://code.visualstudio.com)
    * Install the following extensions:
        * Remote - Containers
        * Better C++ Syntax

## Configuration
### git
Configuring git is what will allow you to submit your homework. While I expect most submissions to
occur from within the container, it would be a good idea to be able to submit from your local OS.
Configuring git locally will also allow VS Code to configure git in the container for you.

* Run the following commands in the terminal, minus the \<\> brackets, and filling in your
information:
    * `git config --global user.name "<FIRST & LAST NAME>"`
    * Example: `git config --global user.name "Adam Sweeney"`
    * `git config --global user.email "<WSU EMAIL>"`
    * Example: `git config --global user.email "adam.sweeney@wichita.edu"`

### VS Code / Docker
**NOTE:** I recommend reading this portion of the guide through once before starting.

The container we will be getting is fully capable for CLI work out of the box, but it requires a
bit of work to integrate with VS Code.

* Open VS Code **and** your terminal app.
* In VS Code, File -> Open Folder...
    * Choose the folder where you cloned this repository
    * It is likely called "docker_env"
* VS Code will show a prompt in the bottom right corner of the window. Choose to "Reopen in
container."
* The first build may take a few minutes. You'll know it's finished when the box disappears in the
lower right corner of VS Code.
* In your terminal app, run the command `docker image ls`
* You are looking for the image ID of the image created by VS Code.
    * The repository name should start with "vsc-env-"
* Run the following command: `docker tag <IMAGE ID> myenv`
    * Don't type the \<\>
    * You can choose name other than myenv if you want; keep it short and descriptive.
    * Creating a tag for this image will simplify using it in a CLI context, i.e., outside of VS
    Code.

## Basic Usage
### CLI
By default, all things changed in a container go away once the container is closed. This is great
for resetting an app, but less desirable if we're writing our homework in a container. However,
this has been solved by the ability to mount local folders as volumes in our containers.

### Poking around the container in CLI
* Run the following command in your terminal app: `docker run -it myenv`
    * `run`: Runs a command in a new container.
    * `-i`: Short for interactive, it keeps STDIN open.
    * `-t`: Allocates a TTY (**T**ele**TY**pewriter)
* Note that `docker run` always creates a new container.

### Mounting a folder
**NOTE:** You may need to exit your previous container first. The command is `exit`.
* Run the following command in your terminal app:
`docker run -it --mount type=bind,source=<LOCAL FOLDER>,target=/root/myfolder myenv`
    * Don't type the \<\>
    * You can use a different name other than 'myfolder' if you want. For example, naming it
    after the specific assignment you're working on.
    * Example: `docker run -it --mount type=bind,source=/Users/sweenish/tmp,target=/root/tmp myenv`
* Alternative syntax: `docker run -it -v <LOCAL FOLDER>:/root/myfolder`

### That's a lot of typing
There are ways to get around this. The most common solution is to create an alias. Note above that
`docker run` always creates a new container. When we exit a container, they stick around. We can
restart a container that we already mounted a folder into, and our folder will still be there,
allowing us to use a shorter command. Which we could still alias.

* `docker container ls` only shows active containers. Use `docker ps -a` instead.
* Find the container ID of the container you mounted volumes into. This may not be obvious if you've
created a lot of containers. You can run `docker container prune` to wipe out all inactive
containers, `docker run` a new container with the folder mounted, and then have an easier time
getting the container ID.
* Once you know the container ID, you can run/alias `docker start -ai <CONTAINER ID>`
    * Don't type the \<\>

### VS Code
In VS Code, always open folders, not files. Opening folders allows the extensions to work properly.
Any folder you open, make sure it has the `.devcontainer` folder from this repository so that VS
Code can use the container. You can copy the folder to the homework directory you created above, or
copy it into whatever folders you're working in.
